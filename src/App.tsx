import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Navigation } from './components/Navigation'
import { ViewRouter } from './router/ViewRouter'
import { routes } from './router/routes'

const App = () => (
  <Router>
    <div className="App">
      <Navigation routes={routes} />

      <ViewRouter routes={routes} />
    </div>
  </Router>
)

export default App
