import { Link } from 'react-router-dom'
import React from 'react'
import { AppRoute } from '../router/routerTypes'

export const Navigation = ({ routes }: { routes: AppRoute[] }) => (
  <nav>
    {routes.map(route => {
      return (
        <div key={route.key}>
          <Link
            to={{
              pathname: route.path,
            }}
          >
            {route.key}
          </Link>
        </div>
      )
    })}
  </nav>
)
