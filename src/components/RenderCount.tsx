import React from 'react'

export const RenderCount = () => {
  const renders = React.useRef(0)

  return <span>{++renders.current}</span>
}

