import React from 'react'

export const DebugView = (value: any) => {
  return (
    <pre>{JSON.stringify(value, undefined, 2)}</pre>
  )
}
