import { FC } from 'react'

export interface AppRoute {
  key: string,
  path: string,
  exact?: boolean,
  component: FC
}
