import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { AppRoute } from './routerTypes'

export const ViewRouter = ({ routes }: { routes: AppRoute[] }) => {
  return (
    <Switch>
      {routes.map(route => {
        return (
          <Route
            key={route.key}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        )
      })}
      <Route component={() => <b>Not Found!</b>} />
    </Switch>
  )
}
