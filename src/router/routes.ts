import { Home } from './home/Home'
import { AppRoute } from './routerTypes'
import { Forms } from './forms/Forms'

export const routes: AppRoute[] = [
  {
    key: 'homepage',
    path: '/',
    exact: true,
    component: Home
  },
  {
    key: 'forms',
    path: '/forms',
    component: Forms
  }
]
