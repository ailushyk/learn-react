import React from 'react'
import { ViewRouter } from '../ViewRouter'
import { formsRoutes } from './formsRoutes'

export const Forms = () => {
  return (
    <ViewRouter routes={formsRoutes} />
  )
}
