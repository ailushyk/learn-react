import { AppRoute } from '../routerTypes'
import { FormSubscriptions } from './FormSubscriptions'
import { FormsDashboard } from './FormsDashboard'

export const formsRoutes: AppRoute[] = [
  {
    key: 'forms.dashboard',
    path: '/forms',
    exact: true,
    component: FormsDashboard
  },
  {
    key: 'forms.subscriptions',
    path: '/forms/subscriptions',
    exact: true,
    component: FormSubscriptions
  }
]
