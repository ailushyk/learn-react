import React from 'react'
import { Navigation } from '../../components/Navigation'
import { formsRoutes } from './formsRoutes'

export const FormsDashboard = () => {
  const routes = [...formsRoutes]
  routes.shift()

  return (
    <div>
      <h1>FormsDashboard</h1>
      <Navigation routes={routes} />
    </div>
  )
}
