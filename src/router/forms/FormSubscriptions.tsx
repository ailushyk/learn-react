import React from 'react'
import { Field, Form } from 'react-final-form'
import { RenderCount } from '../../components/RenderCount'
import { DebugView } from '../../components/DebugView'

interface Values {
  firstName: string
  lastName: string
}

const fieldSubscription = { value: true, error: true }

const required = (value: any) => {
  return value ? false : 'Is required'
}

export const FormSubscriptions = () => {
  const handleSubmit = (values: Values) => {console.log(values)}

  return (
    <div>
      <h2>FormSubscriptions</h2>
      <Form
        onSubmit={handleSubmit}
        subscription={{ submitting: true }}
      >
        {({ handleSubmit, ...rest }) => {
          return (
            <form onSubmit={handleSubmit}>
              <RenderCount />

              <Field
                name={'firstName'}
                subscription={fieldSubscription}
                validate={required}
              >
                {({ input, meta }) => (
                  <div className={meta.active ? 'active' : ''}>
                    <div>
                      <input {...input} />
                      <RenderCount />
                    </div>
                    {meta.error && (<span>{meta.error}</span>)}
                    <DebugView value={{ input, meta }} />
                  </div>
                )}
              </Field>

              <Field
                name={'lastName'}
                subscription={fieldSubscription}
                validate={required}
              >
                {({ input, meta }) => (
                  <div className={meta.active ? 'active' : ''}>
                    <div>
                      <input {...input} />
                      <RenderCount />
                    </div>
                    {meta.error && (<span>{meta.error}</span>)}
                    <DebugView value={{ input, meta }} />
                  </div>
                )}
              </Field>

              <button type="submit">Submit</button>
            </form>
          )
        }}
      </Form>
    </div>
  )
}
